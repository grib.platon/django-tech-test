from marshmallow import validate
from marshmallow import fields
from marshmallow import Schema
from marshmallow.decorators import pre_load, post_load

from techtest.authors.models import Author


class AuthorSchema(Schema):
    class Meta(object):
        model = Author

    id = fields.Integer()
    first_name = fields.String(validate=validate.Length(max=255, min=1), required=True)
    last_name = fields.String(validate=validate.Length(max=255, min=1), required=True)

    @pre_load
    def preprocess_data(self, data, **kwargs):
        if "first_name" in data and type(data["first_name"]) == str:
            data["first_name"] = data["first_name"].strip()
        if "last_name" in data and type(data["last_name"]) == str:
            data["last_name"] = data["last_name"].strip()
        return data

    @post_load
    def update_or_create(self, data, **kwargs):
        author, _ = Author.objects.update_or_create(
            id=data.pop("id", None), defaults=data
        )
        return author
