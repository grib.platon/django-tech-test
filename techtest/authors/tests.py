import json

from django.test import TestCase
from django.urls import reverse

from techtest.authors.models import Author


class AuthorsListViewTestCase(TestCase):
    def setUp(self):
        self.url = reverse("authors-list")
        self.author_1 = Author.objects.create(first_name="Fake Fistname 1", last_name="Fake Lastname 1")
        self.author_2 = Author.objects.create(first_name="Fake Fistname 2", last_name="Fake Lastname 2")

    def test_serializes_with_correct_data_shape_and_status_code(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertCountEqual(
            response.json(),
            [
                {
                    "id": self.author_1.id,
                    "first_name": self.author_1.first_name,
                    "last_name": self.author_1.last_name,
                },
                {
                    "id": self.author_2.id,
                    "first_name": self.author_2.first_name,
                    "last_name": self.author_2.last_name,
                }
            ]
        )


class AuthorViewTestCase(TestCase):
    def setUp(self):
        self.author = Author.objects.create(first_name="Fake Fistname 1", last_name="Fake Lastname 1")
        self.url = reverse("author", kwargs={"author_id": self.author.id})
    
    def test_serializes_single_record_with_correct_data_shape_and_status_code(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertCountEqual(
            response.json(),
            {
                "id": self.author.id,
                "first_name": self.author.first_name,
                "last_name": self.author.last_name,
            }
        )
    
    def test_updates_author_and_regions(self):
        # Change names
        payload = {
            "first_name": "Fake Firstname 1 (Modified)",
            "last_name": "Fake Lastname 1 (Modified)",
        }
        response = self.client.put(
            self.url, data=json.dumps(payload), content_type="application/json"
        )
        author = Author.objects.first()
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(author)
        self.assertEqual(Author.objects.count(), 1)
        self.assertDictEqual(
            response.json(),
            {
                "id": author.id,
                "first_name": payload["first_name"],
                "last_name": payload["last_name"],
            },
        )
    
    def test_remove_author(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Author.objects.count(), 0)
