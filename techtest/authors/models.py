from django.db import models


class Author(models.Model):
    first_name = models.CharField(max_length=255, null=False, blank=False)
    last_name = models.CharField(max_length=255, null=False, blank=False)

    # class Meta:
    #     unique_together = ('first_name', 'last_name',)
