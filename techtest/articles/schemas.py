from marshmallow import validate, ValidationError
from marshmallow import fields
from marshmallow import Schema
from marshmallow.decorators import pre_load, post_load
from django.db import transaction, IntegrityError

from techtest.articles.models import Article
from techtest.authors.models import Author  # import the Author model
from techtest.authors.schemas import AuthorSchema  # import the AuthorSchema
from techtest.regions.models import Region
from techtest.regions.schemas import RegionSchema


class ArticleSchema(Schema):
    class Meta(object):
        model = Article

    id = fields.Integer()
    title = fields.String(validate=validate.Length(max=255, min=1), required=True)
    content = fields.String()
    author = fields.Method(
        required=False, serialize="get_author", deserialize="load_author"
    )
    regions = fields.Method(
        required=False, serialize="get_regions", deserialize="load_regions"
    )

    def get_regions(self, article):
        return RegionSchema().dump(article.regions.all(), many=True)

    def load_regions(self, regions):
        region_objects = []
        current_code = None
        current_id = None
        try:
            with transaction.atomic():
                for region in regions:
                    current_id = region.get("id", None)
                    current_code = region.get("code", None)

                    if not (current_id or current_code):
                        raise ValidationError("The article has got invalid region info.")
                    
                    """
                        If region id is notified, get the region info with the id
                            if there is not id, raise error there is no id.
                        If region code is notified and name is not notified, get the region info with the code.
                            if there is not code, raise error there is no code
                        If region code and name are both notified, create new region object.
                            if there is already code and name, raise error there is already code and name
                    """
                    search_pattern = {}

                    if current_id is not None:
                        search_pattern["pk"] = current_id
                        
                    if current_code is not None:
                        search_pattern["code"] = current_code
                    
                    if region.get("name", None) is not None:
                        search_pattern["name"] = region.get("name")
                    
                    region_obj = Region.objects.get_or_create(**search_pattern, defaults=region)[0]
                    region_objects.append(region_obj)
        except IntegrityError:
            error_info = f"code-{ current_code }" if current_code else f"id-{ current_id }"
            raise ValidationError(f"A region with the { error_info } different from the current region info already exists.")
        return region_objects

    def get_author(self, article):
        return AuthorSchema().dump(article.author)
    
    def load_author(self, author):
        return Author.objects.get_or_create(id=author.pop("id", None), defaults=author)[0]
    
    @pre_load
    def preprocess_data(self, data, *args, **kwargs):
        if "title" in data and type(data["title"]) == str:
            data["title"] = data["title"].strip()

        if "content" in data and type(data["content"]) == str:
            data["content"] = data["content"].strip()

        return data

    @post_load
    def update_or_create(self, data, *args, **kwargs):
        regions = data.pop("regions", None)
        article, _ = Article.objects.update_or_create(
            id=data.pop("id", None), defaults=data
        )
        if isinstance(regions, list):
            article.regions.set(regions)
        return article
