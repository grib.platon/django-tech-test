import json

from django.http.response import HttpResponse
from django.http.request import HttpRequest


def json_response(data={}, status=200):
    return HttpResponse(
        content=json.dumps(data), status=status, content_type="application/json"
    )


class JSONParserMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
    
    def __call__(self, request: HttpRequest):
        if request.method in ["GET", "DELETE"]:
            request.json = {}
            return self.get_response(request)
        
        if request.headers.get("content_type") != "application/json":
            return json_response({"error": "Content type must be \"application/json\""}, 400)

        try:
            request.json = json.loads(request.body if request.body != "" else "{}")
        except json.decoder.JSONDecodeError:
            return json_response({"error": "Non valid json data is inputed"}, 400)
        
        response = self.get_response(request)
        return response
